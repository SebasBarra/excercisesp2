import Stack.Stack;
import  org.junit.Test;
import static org.junit.Assert.assertEquals;

public class stackTests {
    @Test
    public void pushAndPopTest(){

        Stack<Integer> list = new Stack<Integer>();
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        list.push(5);
        assertEquals(6, list.push(10));
        list.pop();
        list.pop();
        assertEquals(4, list.size());
        String result = list.toString();
        System.out.println(result);

        Stack<String> listStr = new Stack<String>();
        listStr.push("sebas");
        listStr.push("pepe");
        listStr.push("jose");
        listStr.push("carlos");
        assertEquals(5, listStr.push("juan"));
        listStr.pop();
        listStr.pop();
        assertEquals(3, listStr.size());
        String resultStr = listStr.toString();
        System.out.println(resultStr);
    }
}
