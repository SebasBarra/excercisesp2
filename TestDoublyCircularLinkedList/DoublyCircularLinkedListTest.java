import H.DoublyCircularLinkedList;
import H.List;
import H.Node;
import  org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DoublyCircularLinkedListTest {
    @Test
    public void reverseTest() {


        List<Integer> list = new DoublyCircularLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        assertEquals(3, list.size());

        list.reverse();

        assertEquals(3, list.size());

        int aux = list.get(0);

        assertEquals(3, aux);

        int aux2 = list.get(1);

        assertEquals(2, aux2);

        int aux3 = list.get(2);

        assertEquals(1, aux3);

        List<String> listString = new DoublyCircularLinkedList<>();
        listString.add("sebas");
        listString.add("pepe");
        listString.add("jose");

        assertEquals(3, listString.size());

        listString.reverse();
        assertEquals(3, listString.size());

        String name1 = listString.get(0);
        assertEquals("jose", name1);

        String name2 = listString.get(1);
        assertEquals("pepe", name2);

        String name3 = listString.get(2);
        assertEquals("sebas", name3);
    }
}
