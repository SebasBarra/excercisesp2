package GetNode;

public class Node {
    public int data;
    public Node next = null;


    public Node(){
        next = null;
    }

    public static int getNth(Node n, int index) throws Exception{
        int value =0;
        Node aux = n;
        for(int i=0;i<=index;i++){
            value = aux.data;
            aux = aux.next;
        }
        return value;
    }
}
