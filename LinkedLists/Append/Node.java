package Append;

public class Node {
    int data;
    Node next = null;

    Node(final int data) {
        this.data = data;
    }

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    public static Node append(Node listA, Node listB) {
        if(listA == null){
            return listB;
        }
        if(listB == null){
            return listA;
        }
        Node aux = listA;
        while(aux.next != null){
            aux = aux.next;
        }
        aux.next = listB;
        return listA;
    }
}
