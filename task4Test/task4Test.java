import  org.junit.Test;
import task2.Canvas;
import task4.Calculator;

import static org.junit.Assert.assertEquals;

public class task4Test {

    @Test
    public void sumGroups(){
        int [] numeros = {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9};
        Calculator c = new Calculator(numeros);
        c.sumGroups();
        System.out.println("array definitivo");
        c.showArray(numeros);
    }

    @Test
    public void quitNumbres(){
        int [] numeros = {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9};

        Calculator c = new Calculator(numeros);
        //c.showArray(numeros);
        System.out.println();
        int [] repetidos = {2, 3,4};
        int [] repetidos2 = {2, 2,6};
        c.removeNumbers(repetidos, repetidos2);
        System.out.println();
        c.showArray(numeros);

    }
}
