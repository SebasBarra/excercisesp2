package stack2;

import bags.Main;
import bags.Node;

public class Stack <T extends Comparable<T> >{
    private int size;

    private Node<T> root;

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public Node<T> obtenerNodo(int indice){
        if(isEmpty() || indice>= size){
            return null;
        }
        Node<T> node = root;
        for(int i = 0;i<indice;i++, node = node.getNext());
        return node;
    }

    public int push(T data){
        Node<T> node = new Node<>(data);
        if(isEmpty()){
            root = node;
        }
        else{
            Node<T> last = obtenerNodo(size - 1);
            last.setNext(node);
        }
        size++;
        return size;
    }

    public T pop(){
        T value;
        Node<T> aux1 = root;
        for (int i = 0;i<size-2;i++,aux1 = aux1.getNext());
        int temp = size;
        if(size > 1 && size <= temp+1){
            value = aux1.getNext().getData();
        }
        else {
            value = aux1.getData();
        }
        aux1.setNext(null);
        size = size -1;
        return value;
    }

}
