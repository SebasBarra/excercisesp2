package stack2;


import static stack2.Operator.getTokens;

public class Main {
    public static void main(String[] args) {
        Stack<Character> list = getTokens("(1+1+1)");
        System.out.println(list.size());
        System.out.println(list);
        Operator op = new Operator(list);
        System.out.println(op.doAdd());
    }
}
