package stack2;

public class Operator {
    private Stack<Character> token;

    public Operator(Stack<Character> token) {
        this.token = token;
    }

    public Stack<Character> getToken() {
        return token;
    }

    public void setToken(Stack<Character> token) {
        this.token = token;
    }

    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }

    private int adding(Stack<Character> numbers){
        int total = 0;
        int value = 0;
        for(int i = 0;i<numbers.size();i++){
            value = Character.getNumericValue(numbers.obtenerNodo(i).getData());
            total = total + value;
        }
        return total;
    }

    public int doAdd(){
        Stack<Character> aux = token;
        Stack<Character> temp = new Stack<>();
        Character num = aux.pop();
        while (aux.size()>0){
            if (num !='+' && num !='*' && num !='(' && num !=')') {
                temp.push(num);
            }
            num = aux.pop();
        }
        int total = adding(temp);
        return total;
    }
}
