import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;
import task6.Map;
import task6.Player;

public class task6Test {
    @Test
    public void fill(){
        Player p1 =new Player("pepe");
        String [][] board = new String[10][10];
        Map map = new Map(board, p1);
        assertArrayEquals(new String[][]{{"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},
                                        {"|O|", "|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|","|O|"},}, map.fillMap());
        map.start();
    }

}
