package DoubleQueue;

import queue.Node;

public class Queue <T extends Comparable<T> >{
    private int size;
    private DoubleNode<T> root,end;

    public Queue(){
        root = null;
        end = null;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public void queue(T data){
        DoubleNode<T> node = new DoubleNode<>(data);
        if(isEmpty()){
            root = node;
            size++;
        }
        else{
            DoubleNode<T> aux = root;
            aux.setPrevious(node);
            root = node;
            root.setNext(aux);
            size++;
        }
    }

    public T dequeue(){
        DoubleNode<T> aux = root;
        T value;

        if(isEmpty()){
            return null;
        }
        if(size == 1){
            value = aux.getData();
            root = null;
            size = 0;
        }else {
            for(int i = 0;i<size-2;i++,aux=aux.getNext());
            value = aux.getNext().getData();
            aux.setNext(null);
            size = size-1;
        }
        return value;
    }

    public DoubleNode<T> obtenerNodo(int indice){
        if(isEmpty() || indice>= size){
            return null;
        }
        DoubleNode<T> node = root;
        for(int i = 0;i<indice;i++, node = node.getNext());
        return node;
    }









}
