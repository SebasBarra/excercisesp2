import static org.junit.Assert.*;
import org.junit.*;
import task7.JUArrayList;

import java.util.Optional;

public class task7Test {

    @Test
    public void size(){
        JUArrayList list = new JUArrayList();
        assertEquals(0, list.size());
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        assertEquals(10, list.size());
    }

    @Test
    public void add(){
        JUArrayList list = new JUArrayList();
        assertEquals(true, list.add(1));
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        assertEquals(true, list.add(11));
    }

    @Test
    public void isEmpty(){
        JUArrayList list = new JUArrayList();
        assertEquals(true, list.isEmpty());

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);

        assertEquals(false, list.isEmpty());
    }

    @Test
    public void removeByIndex(){
        JUArrayList list = new JUArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.remove(2);
        assertEquals(9, list.size());
    }

    @Test
    public void clearList(){
        JUArrayList list = new JUArrayList();
        assertEquals(true, list.isEmpty());
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.clear();
        assertEquals(true, list.isEmpty());
        list.add(1);
        list.add(2);
        assertEquals(false, list.isEmpty());
    }

    @Test
    public void removeByObject(){
        JUArrayList list = new JUArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.remove(new Integer(1));
        assertEquals(9, list.size());
    }

}
