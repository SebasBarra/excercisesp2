package task5;

public class Rotator {

    public Object[] rotateToTheRight(Object[] array, int n){
        for(int i =0; i<n; i++){
            Object lastPosition = array[array.length-1];
            for(int j = array.length-1; j>0;j--){
                array[j] = array[j-1];
            }
            array[0] = lastPosition;
        }
        return array;
    }


    //
    public Object[] rotateToTheLeft(Object[] array, int n){
        for(int i =0; i<n; i++){
            Object firstPosition = array[0];
            for(int j = 0;j<array.length-1;j++){
                array[j] = array[j+1];
            }
            array[array.length-1] = firstPosition;
        }
        return array;
    }



    public Object[] rotate(Object[] data, int n) {
        if(n>0){
            data  = rotateToTheRight(data,n);
            System.out.println();
        } else if (n<0) {
            int positive = Math.abs(n); //convierto el negativo a positivo para que funcione en los ciclos
            rotateToTheLeft(data,positive);
        }

        return data;
    }



}
