package task2;

public class Canvas {
    static String [][] board;

    public Canvas(int width, int height) {
        width = width +2;
        height = height+2;
        board = new String[width][height];
        for(int i = 0; i < width; i++){
            for(int j = 0;j<height;j++){
                if(i != 0 && j != 0 && j!=width-1 && i!=height-1){
                    board[i][j] = " ";
                }
                else if(i==0 || i == width-1){
                    board[i][j] = "-";
                }
                else if(i!=0 && i!=width-1){
                    board[i][j] = "|";
                }

            }
        }
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        if(x1 != x2 && y1!=y2){
            for(int i=x1; i<x2; i++){
                for(int j=y1;j<=y2+1;j++){
                    if(i == x1 || i == y2 || j == x1 || j==y2+1){
                        board[i+1][j+1] = "x";
                    }
                }
            }
        }
        else{
            if(y1 == y2){
                for(int i = x1; i<=x2;i++){
                    board[i+1][y1+1]= "x";
                }
            }
            if(x1 == x2){
                for(int j = y1; j<=y2; j++){
                    board[x1+1][j+1] = "x";
                }
            }

        }
        return this;
    }

    public Canvas fill(int x, int y, char ch) {
        return this;
    }



    public String drawCanvas() {
        String draw = "";
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                draw= draw+board[i][j];
            }
            if(i < board.length-1){
                draw = draw + "\n";
            }
        }
        return draw;
    }

}
