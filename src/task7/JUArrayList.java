package task7;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JUArrayList implements List<Integer> {
    private int[] list = new int[10];
    static int count = 0;


    public void showList(){
        for(int i=0;i<list.length;i++){
            System.out.print(list[i]+ " ");
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        boolean empty = true;
        if(count>0){
            empty=false;
        }
        return empty;
    }

    @Override
    public boolean contains(Object o) {

        return false;
    }



    @Override
    public Iterator<Integer> iterator() {

        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        boolean added = false;
        if(count == list.length){
            int []list2 = new int[list.length+10];
            for(int i = 0;i<list.length;i++){
                list2[i] = list[i];
            }
            list = list2;
        }
        list[count] = integer;
        count = count + 1;
        added = true;
        return added;
    }

    @Override
    public boolean remove(Object o) {
        int value = Integer.parseInt(o.toString());
        System.out.println(value);
        int length = count;
        for(int i = 0;i<count;i++){
            if(count == length && list[i] == value){
                remove(i);
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        for(int i = 0;i<count;i++){
            list[i] = 0;
        }
        count = 0;

    }

    @Override
    public Integer get(int index) {
        return list[index];
    }

    @Override
    public Integer set(int index, Integer element) {
        return null;
    }

    @Override
    public void add(int index, Integer element) {

    }

    @Override
    public Integer remove(int index) {
        for(int i = index;i<count-1;i++){
            list[i] = list[i+1];
        }
        count = count -1;
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }
}
