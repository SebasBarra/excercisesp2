package exercise1;

public class People {
    private int age;
    private String name;
    private String lastName;
    private final String GREET="hello";

    public People(int age, String name, String lastName) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
    }

    public String greet(){
        return GREET + " my name is " + getName();
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }
}
