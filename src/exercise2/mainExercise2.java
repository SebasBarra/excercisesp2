package exercise2;

import java.util.ArrayList;
import java.util.List;

public class mainExercise2 {
    public static void main(String[] args) {
        Analyzer colors = new Analyzer();
        List<Color> colorList = new ArrayList<>();

        colorList.add(Color.BLUE);
        colorList.add(Color.BLUE);
        colorList.add(Color.BLACK);
        colorList.add(Color.BLACK);
        colorList.add(Color.RED);
        colorList.add(Color.PURPLE);
        colorList.add(Color.BROWN);
        colorList.add(Color.GREEN);
        colorList.add(Color.YELLOW);

        System.out.println(colors.numberOfDistinctColors(colorList));

        System.out.println(colors.colorOccurrence(colorList, Color.BLUE));

    }
}
