package exercise2;


import java.util.ArrayList;
import java.util.List;

public class Analyzer implements MysteryColorAnalyzer{


    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        ArrayList differentColors = new ArrayList();
        for (int i = 0; i < mysteryColors.size(); i++) {
            Color color = mysteryColors.get(i);
            if (differentColors.indexOf(color) == -1) {
                differentColors.add(color);
            }
        }
        return differentColors.size();
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int quantity = 0;

        for(int i = 0; i < mysteryColors.size(); i++){
            if(mysteryColors.get(i) == color){
                quantity = quantity + 1;
            }
        }
        return quantity;
    }
}
