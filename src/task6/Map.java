package task6;

public class Map implements Movements{
    private String [][] map;
    Player player;

    public Map(String[][] map, Player player) {
        this.map = map;
        this.player = player;
    }

    private void showMap(){
        for(int i = 0;i< map.length;i++){
            for(int j=0;j<map[i].length;j++){
                System.out.print(map[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public String[][] fillMap(){
        for(int i = 0;i< map.length;i++){
            for(int j=0;j<map[i].length;j++){
                map[i][j] = "|O|";
            }
        }
        return map;
    }

    private void setInPositions(){
        int x=0;
        int y=0;
        fillMap();
        int n=5;
        map[player.getPosX()][player.getPosY()]="|P|";
        for(int i = 0;i<n;i++){
            x=(int)(Math.random()*10);
            y=(int)(Math.random()*10);
            if(map[x][y] != "|O|" || map[x][y] !="|P|"){
                map[x][y] ="|X|";
            }
            else{
                i = i-1;
            }
        }
        System.out.println("Original Map:");
        showMap();
    }


    @Override
    public void moveUp() {
        if(player.getPosX()!=0){
            if(map[player.getPosX()-1][player.getPosY()] !="|X|"){
                System.out.println("Move the player up:");
                map[player.getPosX()-1][player.getPosY()] = "|P|";
                map[player.getPosX()][player.getPosY()] = "|O|";
                player.setPosX(player.getPosX()-1);
            }
        }
        showMap();
    }

    @Override
    public void moveDown() {
        if(player.getPosX()< map.length-1){
            if(map[player.getPosX()+1][player.getPosY()] !="|X|"){
                System.out.println("Move the player down:");
                map[player.getPosX()+1][player.getPosY()] = "|P|";
                map[player.getPosX()][player.getPosY()] = "|O|";
                player.setPosX(player.getPosX()+1);
            }
        }
        showMap();
    }

    @Override
    public void moveRight() {
        if(player.getPosY()< map.length-1){
            if(map[player.getPosX()][player.getPosY()+1] !="|X|"){
                System.out.println("Move the player to the right:");
                map[player.getPosX()][player.getPosY()+1]= "|P|";
                map[player.getPosX()][player.getPosY()] = "|O|";
                player.setPosY(player.getPosY()+1);
            }
        }
        showMap();
    }

    @Override
    public void moveLeft() {
        if(player.getPosY()>0){
            if(map[player.getPosX()][player.getPosY()-1] !="|X|"){
                System.out.println("Move the player to the left:");
                map[player.getPosX()][player.getPosY()-1] = "|P|";
                map[player.getPosX()][player.getPosY()] = "|O|";
                player.setPosY(player.getPosY()-1);
            }
        }
        showMap();
    }


    public void start(){
        setInPositions();
        moveUp();
        moveDown();
        moveRight();
        moveLeft();
    }
}
