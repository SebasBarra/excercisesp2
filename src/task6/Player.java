package task6;

public class Player {
    private String nickName;
    private int posX;
    private int posY;

    public Player(String nickName) {
        this.nickName = nickName;
        this.posX = (int)(Math.random()*10);
        this.posY = (int)(Math.random()*10);
    }


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int x) {
        this.posX = x;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int y) {
        this.posY = y;
    }
}
