package task6;

public interface Movements {
    void  moveUp();
    void  moveDown();
    void  moveRight();
    void  moveLeft();
}
