package recursividad;

public class LlamadasEncadenadas {

    public static void doOne() {
        System.out.println("LlamadasEncadenadas | doOne | before");
        doTwo();
        System.out.println("LlamadasEncadenadas | doOne | after");
    }
    public static void doTwo() {
        System.out.println("LlamadasEncadenadas | doTwo | before");
        doThree();
        System.out.println("LlamadasEncadenadas | doTwo | after");
    }
    public static void doThree() {
        System.out.println("LlamadasEncadenadas | doThree | before");
        doFour();
        System.out.println("LlamadasEncadenadas | doThree | after");
    }
    public static void doFour() {
        System.out.println("LlamadasEncadenadas | doFour");
    }
}
