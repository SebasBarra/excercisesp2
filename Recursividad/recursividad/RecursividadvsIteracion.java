package recursividad;

public class RecursividadvsIteracion {

    public static long factorialIter(long numero) {
        long factorial = 1;
        for (long i = numero; i > 1 ; i--) {
            factorial = factorial * i;
        }
        return factorial;
    }
    public static long factorialRecu(long numero) {
        if (numero > 1) // i > 1
            // factorial = factorial * i
            // i--
            return numero * factorialRecu(numero - 1);

        return 1; // factorial = 1, (primer caso)
    }
}
