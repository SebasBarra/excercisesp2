package recursividad;

public class Main {
    public static void main(String[] args) {

        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        //LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion: el problema ocurre porque el metodo whileTrue se llama a si mismo
        //recursivamente sin ninguna condicion que detenga la recursividad.
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion: en este metodo primero imprime en consola el numero que se pasa por parametro,
        // luego verifica que el numero sea mayor que cero,  si esta condicional se cumple se llama al mismo
        // metodo recursivamente pero pasando el numero restado por menos uno, imprimiendo asi en pantalla
        // el numero menos uno hasta que llega a cero, cuando ya no se cumple la condicional el metodo finaliza.
        Contador.contarHasta(10);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));
    }
}
