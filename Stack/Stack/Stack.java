package Stack;

import bags.Node;

public class Stack <T extends Comparable<T> >{

    private int size;

    private Node<T> root;

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty() {
        return root == null;
    }

    private Node<T> obtenerNodo(int indice){
        if(isEmpty() || indice>= size){
            return null;
        }
        Node<T> node = root;
        for(int i = 0;i<indice;i++, node = node.getNext());
        return node;
    }

    public int push(T data){
        Node<T> node = new Node<>(data);
        if(isEmpty()){
            root = node;
        }
        else{
            Node<T> last = obtenerNodo(size - 1);
            last.setNext(node);
        }
        size++;
        return size;
    }

    public void pop(){
        Node<T> aux1 = root;
        for (int i = 0;i<size-2;i++,aux1 = aux1.getNext());
        aux1.setNext(null);
        size = size -1;
    }

}
