package Stack;

public class Node<T> {

    private T data;
    private bags.Node<T> next;

    public Node(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public bags.Node<T> getNext() {
        return next;
    }

    public void setNext(bags.Node<T> node) {
        next = node;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }

    public void setData(T data) {
        this.data = data;
    }
}
