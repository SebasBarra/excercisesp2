import bags.Bag;
import bags.LinkedBag;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class bagsTest {

    @Test
    public void selectionSortTest(){
        System.out.println("Selection Sort: ");
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        System.out.println(bag);
        bag.selectionSort();
        System.out.println(bag);
    }

    @Test
    public void bubbleSort(){
        System.out.println("Bubble Sort: ");
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        System.out.println(bag);
        bag.bubbleSort();
        System.out.println(bag);
    }
}


