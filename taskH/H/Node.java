package H;

public class Node<T> {
    private T data;
    private Node<T> next;

    public Node(T data) { this.data = data;}

    public T getData() { return data;}

    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    //public String toString() { return "data=" + data; }
    public String toString() { return "{data=" + data + ", sig->" + next + "}"; }
    public T getNth(Node<T> n, int index){
        T value = null;
        Node<T> aux = n;
        for(int i=0;i<=index;i++){
            value =  aux.data;
            aux = aux.next;
        }
        return value;
    }


}
