package H;

public interface List<T extends Comparable<T>> {

    int size();

    boolean isEmpty();

    boolean add(T data);

    boolean remove(T data);

    T get(int index);

    void selectionSort();

    void bubbleSort();

    void mergeSort(Node<T> node, int left, int right);

    void reverse();

}
