package H;

import circular.CircularLinkedList;

import java.sql.Array;

public class DoublyCircularLinkedList<T extends Comparable<T> > implements List<T>{

    private int size;
    private Node<T> head;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    private Node<T> getNode(int index) {
        if (isEmpty() || index > size){
            return null;
        }
        Node<T> node = head;
        for (int i = 0; i < index; i++, node = node.getNext()){
        }
        return node;
    }

    @Override
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        if (head == null) {
            head = node;
        } else {
            Node<T> last = head;
            while(last.getNext() != head){last = last.getNext();}
            last.setNext(node);
        }
        node.setNext(head);
        size++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        if (isEmpty()) return false;
        if (head.getData().equals(data)) {
            if (size == 1) head = null;
            else {
                Node<T> last = head;
                while(last.getNext() != head){last = last.getNext();}
                last.setNext(head.getNext());
                head = head.getNext();
            }
            size--;
            return true;
        }
        Node<T> previous = head;
        for (Node<T> current = head.getNext(); current != head; previous = current, current = current.getNext()) {
            if (current.getData().equals(data)) {
                previous.setNext(current.getNext());
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    @Override
    public void selectionSort() {

    }

    @Override
    public void bubbleSort() {

    }

    private void merge(Node<T> node, int a, int b, int c) {
        Node<T> finalNode = node;
        int n1 = b-a+1;
        int n2 = c-b;

        Node<T> aux1 = getNode(n1);
        Node<T> aux2 = getNode(n2);

        int e,f,g;
        e=0;
        f=0;
        g= a;

        while (e<n1 && f<n2){
            if(aux1.getNth(aux1,e).compareTo(aux2.getNth(aux2,f))>=0){
                T value1 = node.getNth(node, e);
                value1 = aux1.getNth(aux1,f);
                //trainer en esta parte sin el setData se complico y no halle
                //una manera de actualizar el data
                e++;
            }
        }
    }


    @Override
    public void mergeSort(Node<T> node, int left, int right) {
        if(left<right){
            int mid = (left+right)/2;
            mergeSort(node, left, mid);
            mergeSort(node, mid+1, right);
            merge(node,left,mid,right);
        }
    }

    public void  clear(){
        head = null;
        size=0;
    }


    @Override
    public void reverse(){
        Node<T> aux = head;
        int auxSize = size;
        Node<T> reversed = getNode(size-1);
        reversed.setNext(reversed);
        Node<T> reversed2 = reversed;
        clear();
        size+=1;
        for(int i =auxSize-2;i>=0;i--){
            reversed2 = reversed2.getNext();
            reversed2.setNext(new Node<>(aux.getNth(aux,i)));
            size++;
        }

        reversed.setNext(reversed2);
        Node<T> aux2 = reversed;
        while (aux2.getNext()!=null){
            aux2 =aux2.getNext();
        }
        aux2.setNext(reversed);

        head = reversed;
    }




}
