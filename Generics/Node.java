public class Node<T> {
    T data;
    Node<T> next;
    public Node(T data) {
        this.data = data;
    }
    public T getData() {
        return data;
    }
    public Node<T> getNext() {
        return next;
    }
    public void setNext(Node<T> node) {
        next = node;
    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }

    public void setData(T data) {
        this.data = data;
    }
}
