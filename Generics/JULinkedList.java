import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T> implements List<T> {
    static int size;
    private Node<T> root;

    public JULinkedList(){
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root==null;
    }

    @Override
    public boolean contains(Object o) {
        boolean result = false;
        Node<T> node = root;
        for(int i = 0;i<size;i++,node = node.next){
            if(node.data == o){
                result = true;
            }
        }
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    private Node<T> getNode(int index) {
        if (isEmpty() || index > size){
            return null;
        }
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.next){

        }
        return node;
    }

    @Override
    public boolean add(T t) {
        Node<T> node = new Node<>(t);
        if(isEmpty()){
            root = node;
            size = size+1;
        }
        else{
            Node<T> last = getNode(size-1);
            last.next = node;
            size = size+1;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> previous = root;
        for (Node<T> actual = root; actual != null; previous = actual, actual = actual.next) {
            if (actual.data == o) {
                System.out.println("a");
                if (root == actual){
                    root = actual.next;
                }
                else{
                    previous.next = actual.next;
                }
                size = size -1;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index > size){
            return null;
        }
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.next){

        }
        T value = node.data;

        return value;
    }

    @Override
    public T set(int index, T element) {
        Node<T> node = getNode(index);
         node.data = element;
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    @Override
    public T remove(int index) {
        T node = get(index);
        this.remove(node);
        return null;
    }

    @Override
    public int indexOf(Object o) {
        Node<T> node = root;
        int position = 0;
        for (int i = 0; i<size; i++, node = node.next){
            if(node.data == o){
                position = i;
                break;
            }
        }
        System.out.println(position);
        return position;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> node = root;
        int position = 0;
        for (int i = 0; i<size; i++, node = node.next){
            if(node.data == o){
                position = i;
            }
        }
        System.out.println(position);
        return position;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}
