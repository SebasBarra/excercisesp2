package circular;

public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    private Node<T> getNode(int index) {
        if (isEmpty() || index > size){
            return null;
        }
        Node<T> node = head;
        for (int i = 0; i <= index; i++, node = node.getNext()){
        }
        return node;
    }

    @Override
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        if(isEmpty()){
            head = node;
            head.setNext(head);
            size = size+1;
        }
        else{
            Node<T> last =  getNode(size);
            Node<T> aux = last.getNext();
            node.setNext(head);
            aux.setNext(node);
            size = size+1;

        }
        return true;
    }

    @Override
    public boolean remove(T data) {
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0){
            return null;
        }
        else {
            Node<T> node = head;
            if(index>=size){
                for (int i = 0; i <= index ; i++,node = node.getNext()){

                    if(i == index-1){

                        node = head;
                    }
                }
                return node.getData();
            }
            else {
                if(index == size-1){

                    for (int i = 0; i <= index+1 ; i++,node = node.getNext()){

                    }
                }
                else{
                    for (int i = 0; i < index ; i++,node = node.getNext()){
                    }
                }
                return node.getData();
            }
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }
}
