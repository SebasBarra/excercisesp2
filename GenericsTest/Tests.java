import static org.junit.Assert.*;
import org.junit.*;

import java.util.List;
import java.util.Optional;

public class Tests {

    @Test
    public void sizeTest(){
        List<Integer> list = new JULinkedList<>();
        assertEquals(0, list.size());
    }


    @Test
    public void isEmptyTest(){
        List<Integer> list = new JULinkedList<>();
        assertEquals(true, list.isEmpty());
    }

    @Test
    public void addTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(3, list.size());
        assertEquals(true, list.add(4));
        assertEquals(4, list.size());
    }

    @Test
    public void removeTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.remove((Object)3);
        assertEquals(2, list.size());
    }

    @Test
    public void clearTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.clear();
        assertEquals(true, list.isEmpty());
    }

    @Test
    public void getTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        int value = list.get(1);
        assertEquals(2, value);
    }

    @Test
    public void removeByIndexTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.remove(2);
        assertEquals(2, list.size());
    }

    @Test
    public void containsTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(true, list.contains(2));
    }

    @Test
    public void setTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.set(1, 4);
        int value = list.get(1);
        assertEquals(4, value);
    }

    @Test
    public void indexOfTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(2);
        int index = list.indexOf((Object)2);
        assertEquals(1, index);
    }

    @Test
    public void lastIndexOfTest(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(2);
        int lastIndex = list.lastIndexOf((Object)2);
        assertEquals(3, lastIndex);
    }

}
