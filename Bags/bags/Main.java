package bags;

public class Main {
    public static void main(String[] args) {
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        System.out.println(bag);
    }
}
