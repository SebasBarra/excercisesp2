package bags;

import java.awt.*;

public class LinkedBag <T extends Comparable<T> > implements Bag<T>{

    private int size;
    private Node<T> root;

    @Override
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public boolean isEmpty() {
        return root == null;
    }

    private Node<T> getNode(int index) {
        if (isEmpty() || index > size){
            return null;
        }
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.getNext()){
        }
        return node;
    }



    @Override
    public void selectionSort(){
        Node<T> node = root;
        for (int i = 0; i < size - 1; i++, node = node.getNext()){
            for (int j = i + 1; j < size; j++){
                T a = getNode(i).getData();
                T b = getNode(j).getData();
                if(a.compareTo(b)>0){
                    T temp = getNode(i).getData();
                    Node<T> aux = getNode(i);
                    aux.setData(b);
                    Node<T> aux2 = getNode(j);
                    aux2.setData(temp);
                }
            }
        }
    }


    @Override
    public void bubbleSort() {
        Node<T> aux1;
        Node<T>  aux2 = root;

        while (aux2 != null) {
            aux1 = aux2.getNext();
            while (aux1 != null) {
                T a = aux1.getData();
                T b = aux2.getData();
                if (a.compareTo(b) < 0){
                    T aux = aux2.getData();
                    aux2.setData(aux1.getData());
                    aux1.setData(aux);
                }
                aux1 = aux1.getNext();
            }
            aux2 = aux2.getNext();
        }
    }

    @Override
    public void xchange(int y, int x) {
        Node<T> xNode=root;
        Node<T> yNode=root;

        for(int i=0;i<x;i++,xNode = xNode.getNext());
        for(int j=0;j<y;j++,yNode = yNode.getNext());

        T aux = xNode.getData();
        xNode.setData(yNode.getData());
        yNode.setData(aux);
    }
}
