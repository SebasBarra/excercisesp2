package bags;

public interface Bag<T extends Comparable<T> > {

    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    void xchange(int y, int x);

}
