import  org.junit.Test;
import static org.junit.Assert.assertEquals;
import DoubleQueue.DoubleNode;
import DoubleQueue.Queue;

import java.util.Optional;

public class QueueTests {
    @Test
    public void queueTest(){
        Queue<Integer> queue = new Queue<>();
        queue.queue(1);
        queue.queue(2);
        queue.queue(3);
        queue.queue(4);
        queue.queue(5);
        assertEquals(5, queue.size());
        System.out.println(queue.obtenerNodo(4).getData());
    }

    @Test
    public void dequeueTest(){
        Queue<Integer> queue = new Queue<>();
        queue.queue(1);
        queue.queue(2);
        queue.queue(3);
        queue.queue(4);
        queue.queue(5);

        int value = queue.dequeue();
       assertEquals(1,value);
       assertEquals(4,queue.size());

       int value2 = queue.dequeue();
        assertEquals(2,value2);
        assertEquals(3,queue.size());
    }

    @Test
    public void queueAndDequeueTest(){
        Queue<Integer> queue = new Queue<>();
        queue.queue(1);
        queue.queue(2);
        queue.queue(3);
        queue.queue(4);
        queue.queue(5);
        assertEquals(5, queue.size());
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        int value = queue.dequeue();
        assertEquals(0, queue.size());

        assertEquals(5,value);

        assertEquals(true,queue.isEmpty());

        System.out.println(queue.obtenerNodo(0));

        queue.queue(10);
        int data = queue.obtenerNodo(0).getData();
        assertEquals(10, data);
    }
}
