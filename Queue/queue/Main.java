package queue;

public class Main {


    public static void main(String[] args) {
        Queue<Integer> list = new Queue<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);

        System.out.println(list.deque());
        System.out.println(list);

        System.out.println(list.deque());
        System.out.println(list);

        System.out.println(list.deque());
        System.out.println(list);

        System.out.println(list.deque());
        System.out.println(list);

        System.out.println(list.deque());
        System.out.println(list);

        System.out.println(list.deque());
        System.out.println(list);


        /*Queue<Integer> list1 = new Queue<>();
        Queue<Integer> list2 = new Queue<>();
        list1.add(1);
        list1.add(3);
        list1.add(5);
        list2.add(2);
        list2.add(4);
        list2.add(6);

        Queue<Integer> auxlist = new Queue<>();
        Queue<Integer> finallist = auxlist.intercalate(list2,list1);
        System.out.println(finallist);

        Queue<Integer> list3 = new Queue<>();
        list3.addNoDuplicates(1);
        list3.addNoDuplicates(2);
        list3.addNoDuplicates(3);
        list3.addNoDuplicates(4);
        System.out.println(list3);
        list3.addNoDuplicates(2);
        System.out.println(list3);
        list3.addNoDuplicates(1);
        System.out.println(list3);
        list3.addNoDuplicates(4);*/
    }
}
