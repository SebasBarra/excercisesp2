package queue;

public class Queue <T extends Comparable<T> >{

    private int size;
    private Node<T> root;

    private Node<T> head;

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public void add(T data){
        Node<T> node = new Node<>(data);
        if(isEmpty()){
            root = node;
            head = node;
            size++;
        }
        else {
            Node<T> aux = root;
            root = node;
            root.setNext(aux);
            size++;
        }
    }

    public T deque(){
        Node<T> aux = root;
        T value;

        if(isEmpty()){
            return null;
        }

        if(size == 1){
            value = aux.getData();
            root = null;
            size = size-1;
        }
        else{
            for(int i = 0;i<size-2;i++,aux=aux.getNext());
            value = aux.getNext().getData();
            aux.setNext(null);
            size = size-1;
        }
        return value;
    }

    public Queue<T> intercalate(Queue<T> list1, Queue<T> list2){
        Queue<T> finalList = new Queue<>();
        while (list1.isEmpty()!= true  && list2.isEmpty()!=true){
            finalList.add(list2.deque());
            finalList.add(list1.deque());
        }
        return finalList;
    }


    public void addNoDuplicates(T data){
        Node<T> node = new Node<>(data);
        if(isEmpty()){
            root = node;
            head = node;
            size++;
        }
        else {
            Node<T> temp = root;
            boolean duplicate = false;
            for(int i = 0;i<size;i++,temp=temp.getNext()){
                if(temp.getData() == data){
                    duplicate = true;
                }
            }
            if(duplicate){
                System.out.println("el elemento "+ data + " ya se encuentra en la cola");
            }
            else {
                Node<T> aux = root;
                root = node;
                root.setNext(aux);
                size++;
            }
        }
    }


}
